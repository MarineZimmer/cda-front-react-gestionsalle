package fr.afpa.salleafpa.dao.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.Proxy;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(of= {"login","mdp"})

@Entity
@Table(name = "authentification")
public class AuthentificationDao {

	@Id
	@Column (name="login", updatable = false, nullable = false, length = 50 )
	String login;
	
	@Column (name="mdp", updatable = true, nullable = false,length = 50)
	String mdp;
	
	@OneToOne (mappedBy = "authentification")
	PersonneDao personne;
}
