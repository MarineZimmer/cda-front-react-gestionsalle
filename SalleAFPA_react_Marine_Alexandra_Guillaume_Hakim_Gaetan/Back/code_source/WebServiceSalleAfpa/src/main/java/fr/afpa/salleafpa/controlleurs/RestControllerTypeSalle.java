package fr.afpa.salleafpa.controlleurs;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.salleafpa.jwt.JwtUtils;
import fr.afpa.salleafpa.metier.entities.Batiment;
import fr.afpa.salleafpa.metier.entities.TypeMateriel;
import fr.afpa.salleafpa.metier.entities.TypeSalle;
import fr.afpa.salleafpa.metier.iservices.IServiceTypeSalle;
import fr.afpa.salleafpa.outils.ControleSaisie;
import fr.afpa.salleafpa.outils.Parametrage;

@RestController

@CrossOrigin(origins = "*")
public class RestControllerTypeSalle {

	private static final Logger logger = LoggerFactory.getLogger(RestControllerTypeSalle.class);

	@Autowired
	private IServiceTypeSalle servTypeSalle;
	
	/*
	@GetMapping(value = "/typesalle/{id}")
	public ResponseEntity<?> getTypeSalleWS(@PathVariable("id") Integer id,@RequestHeader HttpHeaders header) {
		
		
		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), false)) {
				TypeSalle typeSalle =  servTypeSalle.get(id);
				if(typeSalle!=null) {
				status = HttpStatus.OK;
				return ResponseEntity.status(status).body(typeSalle);
				}else {
					status = HttpStatus.INTERNAL_SERVER_ERROR;
					return ResponseEntity.status(status).body("erreur");
				}
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized"); 
	
	}*/

	@GetMapping(value = "/typesalle")
	public ResponseEntity<?> getListeTypeSallesWS(@RequestHeader HttpHeaders header) {
		
		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), false)) {
				List<TypeSalle> listeTypeSalle = servTypeSalle.getAll();
				if(listeTypeSalle!=null) {
				status = HttpStatus.OK;
				return ResponseEntity.status(status).body(listeTypeSalle);
				}else {
					status = HttpStatus.INTERNAL_SERVER_ERROR;
					return ResponseEntity.status(status).body("erreur");
				}
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized"); 
		
	}
	
	@GetMapping(value = "/typesalle/{page}")
	public ResponseEntity<?> getListeTypeSallesPagination(@PathVariable("page") String page, @RequestHeader HttpHeaders header) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		HttpStatus status;
		
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		
		if (headerAutorisation.isPresent()) {
			
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			
			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), false)) {
				
				int nbTypeSallePage = 1;
				if (ControleSaisie.isIntegerPositif(page)) {
					nbTypeSallePage = Integer.parseInt(page) - 1;
				}
				if (nbTypeSallePage < 0) {
					nbTypeSallePage = 0;
				}
				
				
				int nbPage = servTypeSalle.nbPageTypeSalle(Parametrage.NB_TYPE_MATERIEL_PAGE);
				
				if (nbTypeSallePage > nbPage) {
					nbTypeSallePage = nbPage-1;
				}
				
				List<TypeSalle> listeTypeSalle = servTypeSalle.getAllTypeSalle(nbTypeSallePage, Parametrage.NB_TYPE_MATERIEL_PAGE);
				map.put("listeObjet", listeTypeSalle);
				map.put("nbPage", nbPage);

			
				if(listeTypeSalle!=null) {
				status = HttpStatus.OK;
				return ResponseEntity.status(status).body(map);
				
				}else {
					status = HttpStatus.INTERNAL_SERVER_ERROR;
					return ResponseEntity.status(status).body("erreur");
				}
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized"); 
		
	}
	

	@PostMapping(value = "/typesalle")
	public ResponseEntity<?> creationTypeSalleWS(@RequestBody TypeSalle typeSalle,@RequestHeader HttpHeaders header) {
		
		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), true)) {
				TypeSalle typeSalleCree = servTypeSalle.creation(typeSalle);
				if(typeSalleCree!=null) {
				status = HttpStatus.OK;
				return ResponseEntity.status(status).body(typeSalleCree);
				}else {
					status = HttpStatus.BAD_REQUEST;
					return ResponseEntity.status(status).body("erreur");
				}
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized");
		

	}
	
	@PutMapping(value = "/typesalle")
	public ResponseEntity<?> updateTypeSalleWS(@RequestBody TypeSalle typeSalle,@RequestHeader HttpHeaders header) {
		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), true)) {
				TypeSalle typeSalleUpdate = servTypeSalle.update(typeSalle);
				if (typeSalleUpdate != null) {
					status = HttpStatus.OK;
					return ResponseEntity.status(status).body(typeSalleUpdate);
				} else {
					status = HttpStatus.BAD_REQUEST;
					return ResponseEntity.status(status).body("non modifié");
				}
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized");
	}

	

	@DeleteMapping(value = "/typesalle/{id}")
	public ResponseEntity<?> deleteTypeSalleWS( @PathVariable("id") Integer id,@RequestHeader HttpHeaders header) {
		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), true)) {
				TypeSalle typeSalleSuppr = servTypeSalle.delete(id);
				if (typeSalleSuppr != null) {
					status = HttpStatus.OK;
					return ResponseEntity.status(status).body(typeSalleSuppr);
				} else {
					status = HttpStatus.BAD_REQUEST;
					return ResponseEntity.status(status).body("non supprimé");
				}
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized");
		 
	}

}
