package fr.afpa.salleafpa.metier.iservices;

import java.util.List;

import fr.afpa.salleafpa.metier.entities.TypeMateriel;

public interface IServiceTypeMateriel{

	TypeMateriel creationTypeMateriel(TypeMateriel typeMateriel);

	TypeMateriel updateTypeMateriel(TypeMateriel typeMateriel);

	TypeMateriel getTypeMateriel(Integer id);

	List<TypeMateriel> getAllTypeMateriel();

	TypeMateriel deleteTypeMateriel(Integer id);

	int nbPageListeMateriel(int nbTypeMaterielPage);
	
	List<TypeMateriel> getAllTypeMateriel(int page, int nbTypeMaterielPage);
}

