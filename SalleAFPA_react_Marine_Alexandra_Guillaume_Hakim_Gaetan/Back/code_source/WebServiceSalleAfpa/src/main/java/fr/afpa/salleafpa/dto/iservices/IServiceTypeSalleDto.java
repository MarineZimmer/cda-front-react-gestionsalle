package fr.afpa.salleafpa.dto.iservices;

import java.util.List;

import fr.afpa.salleafpa.metier.entities.TypeSalle;

public interface IServiceTypeSalleDto {

	TypeSalle creation(TypeSalle typeSalle);

	TypeSalle update(TypeSalle typeSalle);

	TypeSalle get(Integer id);

	List<TypeSalle> getAll();

	TypeSalle delete(Integer id);

	int nbPageTypeSalle(int nbPageTypeSalle);


	List<TypeSalle> getAllTypeSalle(int page, int nbPageTypeSalle);
}
