package fr.afpa.salleafpa.controlleurs;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.salleafpa.jwt.JwtUtils;
import fr.afpa.salleafpa.metier.entities.Batiment;
import fr.afpa.salleafpa.metier.entities.Reservation;
import fr.afpa.salleafpa.metier.entities.Role;
import fr.afpa.salleafpa.metier.entities.TypeMateriel;
import fr.afpa.salleafpa.metier.iservices.IServiceTypeMateriel;
import fr.afpa.salleafpa.outils.ControleSaisie;
import fr.afpa.salleafpa.outils.Parametrage;

@RestController

@CrossOrigin(origins = "*")
public class RestControllerTypeMateriel {

	private static final Logger logger = LoggerFactory.getLogger(RestControllerTypeMateriel.class);

	@Autowired
	private IServiceTypeMateriel servtypeMateriel;
	
	/*
	@GetMapping(value = "/typemateriel/{id}")
	public  ResponseEntity<?> getTypeMaterielWS(@PathVariable("id") Integer id,@RequestHeader HttpHeaders header) {
		
		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), false)) {
				TypeMateriel typeMateriel =  servtypeMateriel.getTypeMateriel(id);
				if(typeMateriel!=null) {
				status = HttpStatus.OK;
				return ResponseEntity.status(status).body(typeMateriel);
				}else {
					status = HttpStatus.INTERNAL_SERVER_ERROR;
					return ResponseEntity.status(status).body("erreur");
				}
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized"); 
	
	}*/

	@GetMapping(value = "/typemateriel")
	public  ResponseEntity<?> getListeTypeMaterielsWS(@RequestHeader HttpHeaders header) {
		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		if (headerAutorisation.isPresent()) {
			
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			
			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), false)) {
				
				List<TypeMateriel> listeTypeMateriel = servtypeMateriel.getAllTypeMateriel();
				
				if(listeTypeMateriel!=null) {
					
				status = HttpStatus.OK;
				return ResponseEntity.status(status).body(listeTypeMateriel);
				}else {
					status = HttpStatus.INTERNAL_SERVER_ERROR;
					return ResponseEntity.status(status).body("erreur");
				}
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized"); 
	}
	
	@GetMapping(value = "/typemateriel/{page}")
	public  ResponseEntity<?> getListeTypeMaterielsPagination(@PathVariable("page") String page, @RequestHeader HttpHeaders header) {
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		HttpStatus status;
		
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		
		if (headerAutorisation.isPresent()) {
			
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			
			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), false)) {
				

				int nbTypeMatPage = 1;
				if (ControleSaisie.isIntegerPositif(page)) {
					nbTypeMatPage = Integer.parseInt(page) - 1;
				}
				if (nbTypeMatPage < 0) {
					nbTypeMatPage = 0;
				}
				
				int nbPage = servtypeMateriel.nbPageListeMateriel(Parametrage.NB_TYPE_MATERIEL_PAGE);
				
				if (nbTypeMatPage > nbPage) {
					nbTypeMatPage = nbPage-1;
				}
				
				List<TypeMateriel> listeTypeMateriel = servtypeMateriel.getAllTypeMateriel(nbTypeMatPage, Parametrage.NB_TYPE_MATERIEL_PAGE);
				map.put("listeObjet", listeTypeMateriel);
				map.put("nbPage", nbPage);

				if (listeTypeMateriel != null) {
					status = HttpStatus.OK;
					return ResponseEntity.status(status).body(map);
					
				}else {
					status = HttpStatus.INTERNAL_SERVER_ERROR;
					return ResponseEntity.status(status).body("erreur");
				}
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized"); 
	}
	
	
	
	
	
	

	@PostMapping(value = "/typemateriel")
	public  ResponseEntity<?> creationTypeMaterielWS(@RequestBody TypeMateriel typeMateriel,@RequestHeader HttpHeaders header) {
		
		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), true)) {
				TypeMateriel typeMaterielCréé = servtypeMateriel.creationTypeMateriel(typeMateriel);
				if(typeMaterielCréé!=null) {
				status = HttpStatus.OK;
				return ResponseEntity.status(status).body(typeMaterielCréé);
				}else {
					status = HttpStatus.BAD_REQUEST;
					return ResponseEntity.status(status).body("erreur");
				}
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized");
		 

	}

	@PutMapping(value = "/typemateriel")
	public  ResponseEntity<?> updateTypeMaterielWS(@RequestBody TypeMateriel typeMateriel,@RequestHeader HttpHeaders header) {
		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), true)) {
				TypeMateriel typeMaterielUpdate = servtypeMateriel.updateTypeMateriel(typeMateriel);
				if (typeMaterielUpdate != null) {
					status = HttpStatus.OK;
					return ResponseEntity.status(status).body(typeMaterielUpdate);
				} else {
					status = HttpStatus.BAD_REQUEST;
					return ResponseEntity.status(status).body("non modifié");
				}
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized");

	}


	@DeleteMapping(value = "/typemateriel/{id}")
	public  ResponseEntity<?> deleteTypeMaterielWS( @PathVariable("id") Integer id,@RequestHeader HttpHeaders header) {
		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));
		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));
			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), true)) {
				TypeMateriel typematerielSuppr = servtypeMateriel.deleteTypeMateriel(id);
				if (typematerielSuppr != null) {
					status = HttpStatus.OK;
					return ResponseEntity.status(status).body(typematerielSuppr);
				} else {
					status = HttpStatus.BAD_REQUEST;
					return ResponseEntity.status(status).body("non supprimé");
				}
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized");
		
		
		 


	}

}
