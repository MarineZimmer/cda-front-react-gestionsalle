package fr.afpa.salleafpa.controlleurs;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.salleafpa.jwt.JwtUtils;
import fr.afpa.salleafpa.metier.entities.Reservation;
import fr.afpa.salleafpa.metier.entities.Salle;
import fr.afpa.salleafpa.metier.iservices.IServiceFiltreSalle;
import fr.afpa.salleafpa.metier.iservices.IServiceReservation;
import fr.afpa.salleafpa.metier.iservices.IServiceSalle;
import fr.afpa.salleafpa.metier.iservices.IServiceTypeMateriel;
import fr.afpa.salleafpa.metier.iservices.IServiceTypeSalle;
import fr.afpa.salleafpa.outils.ControleSaisie;
import fr.afpa.salleafpa.outils.Parametrage;

@RestController
@CrossOrigin(origins = "*")
public class RestControllerReservationSalle {

	private static final Logger logger = LoggerFactory.getLogger(RestControllerReservationSalle.class);

	@Autowired
	private IServiceSalle servSalle;

	@Autowired
	private IServiceTypeMateriel servTypeMateriel;

	@Autowired
	private IServiceTypeSalle servTypeSalle;

	@Autowired
	private IServiceFiltreSalle servFiltreSalle;

	@Autowired
	private IServiceReservation servReservation;

	@GetMapping(value = "/reservation")
	public ResponseEntity<?> reservationsGetAll(@RequestHeader HttpHeaders header) {

		Map<String, Object> map = new HashMap<String, Object>();
		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));

		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));

			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), true)) {

				List<Reservation> listeReservation = servReservation.getAll();
				map.put("listeReservation", listeReservation);

				if (listeReservation != null) {
					status = HttpStatus.OK;
					return ResponseEntity.status(status).body(map);
				} else {
					status = HttpStatus.INTERNAL_SERVER_ERROR;
					return ResponseEntity.status(status).body("erreur");
				}
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized");

	}

	@GetMapping(value = "/reservationpage/{page}")
	public ResponseEntity<?> reservationsGet(@PathVariable("page") String page, @RequestHeader HttpHeaders header) {

		Map<String, Object> map = new HashMap<String, Object>();
		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));

		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));

			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), true)) {

				int nbReservPage = 1;
				if (ControleSaisie.isIntegerPositif(page)) {
					nbReservPage = Integer.parseInt(page) - 1;
				}
				int nbPage = servReservation.nbPageListeReservation(Parametrage.NB_RESERV_PAGE);
				if (nbReservPage < 0) {
					nbReservPage = 0;
				}else if (nbPage>0 && nbReservPage>nbPage-1) {
					nbReservPage=nbPage-1;
				}
			

				List<Reservation> listeReservation = servReservation.getAll(nbReservPage, Parametrage.NB_RESERV_PAGE);
				map.put("listeReservation", listeReservation);
				map.put("nbPage", nbPage);

				if (listeReservation != null) {
					status = HttpStatus.OK;
					return ResponseEntity.status(status).body(map);
				} else {
					status = HttpStatus.INTERNAL_SERVER_ERROR;
					return ResponseEntity.status(status).body("erreur");
				}
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized");
	}

	@RequestMapping(value = "/reservation/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> reservation2Get(@PathVariable("id") Integer id, @RequestHeader HttpHeaders header) {

		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));

		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));

			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), true)) {

				Reservation reservation = servReservation.getById(id);

				if (reservation == null) {
					status = HttpStatus.NOT_FOUND;

				} else {
					status = HttpStatus.INTERNAL_SERVER_ERROR;
					return ResponseEntity.status(status).body("erreur");
				}

			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized");
	}

	/**
	 * web service creation reservation
	 * 
	 * @param mv        le ModelAndView
	 * @param session   : la session
	 * @param id        : l'id de la salle à réserver
	 * @param dateDebut : la date de début
	 * @param dateFin   : la date fin
	 * @param nom       : le nom de la réservation
	 * @return : le ModelAndView correspondant au récapitulatif de la réservation
	 */
	@PostMapping(value = "/reservation")
	public ResponseEntity<?> reservationpost(@RequestBody Reservation reservation, @RequestHeader HttpHeaders header) {

		Salle salle = null;

		boolean ok = true;
		Object reponse;
		Map<String, String> map = new HashMap<String, String>();
		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));

		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));

			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), true)) {

				if (!ControleSaisie.isDateReservation("" + reservation.getDateDebut(), "" + reservation.getDateFin())) {
					ok = false;
					map.put("dateKO", "* Dates invalides");
				}

				if (reservation.getSalle() != null) {
					salle = servSalle.getSalle(reservation.getSalle().getIdSalle());
				}
				if (salle == null) {
					ok = false;
				} else {
					if (servFiltreSalle.etatSalle(salle, reservation.getDateDebut(), reservation.getDateFin(), false)) {
						ok = false;
						map.put("occupeKO", "* salle occupée durant cette période");
					}
					if (!salle.isActif()) {
						ok = false;
						map.put("actifKO", "* salle inactif");
					}
				}

				if (!ControleSaisie.isNonVide(reservation.getNomReservation())) {
					ok = false;
					map.put("nomKO", "* veuillez entrer un nom de réservation");
				}
				if (ok) {
					reservation.setSalle(salle);
					reponse = servReservation.createReservation(reservation);
					if (reponse != null) {
						status = HttpStatus.OK;
						return ResponseEntity.status(status).body(reponse);
					} else {
						status = HttpStatus.INTERNAL_SERVER_ERROR;
						return ResponseEntity.status(status).body("erreur");
					}

				} else {
					status = HttpStatus.BAD_REQUEST;
					reponse = map;
					return ResponseEntity.status(status).body(reponse);
				}

			}
		}

		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized");
	}

	/**
	 * web service modification reservation
	 * 
	 * @param mv        : le modelAndView
	 * @param session   : la session
	 * @param dateDebut : la date de début de la réservation
	 * @param dateFin:  la date de fin de la réservation
	 * @param nom       : le nom de la réservation
	 * @param id        : l'id de la réservation
	 * @return le ModelAndView de la visualisation de la réservation
	 */
	@PutMapping(value = "/reservation")
	public ResponseEntity<?> modificationReservation(@RequestBody Reservation reservation,
			@RequestHeader HttpHeaders header) {

		boolean ok = true;
		Object reponse;
		Map<String, String> map = new HashMap<String, String>();
		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));

		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));

			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), true)) {

				Reservation reservationAncienne = servReservation.getById(reservation.getId());

				if (!ControleSaisie.isNonVide(reservation.getNomReservation())) {
					ok = false;
					map.put("nomKO", "* veuillez entrer un nom de réservation");
				}

				if (reservationAncienne != null) {
					if (!ControleSaisie.isDateReservationModification("" + reservation.getDateDebut(),
							"" + reservation.getDateFin(), reservationAncienne.getDateDebut())) {
						ok = false;
						map.put("dateKO", "* Dates invalides");
					}

					reservationAncienne.getSalle().getListeReservation().remove(reservationAncienne);
					if (servFiltreSalle.etatSalle(reservationAncienne.getSalle(), reservation.getDateDebut(),
							reservation.getDateFin(), false)) {
						ok = false;
						map.put("reservationKO", "La salle est déjà occupée pendant cette période");
					}

					if (ok) {
						reservation.setSalle(reservationAncienne.getSalle());
						reponse = servReservation.update(reservation);
						if (reponse != null) {
							status = HttpStatus.OK;
							return ResponseEntity.status(status).body(reponse);
						} else {
							status = HttpStatus.INTERNAL_SERVER_ERROR;
							return ResponseEntity.status(status).body("erreur");
						}

					} else {
						status = HttpStatus.BAD_REQUEST;
						map.put("modifKO", "Désolé la reservation n'a pas été modifié");
						reponse = map;
						return ResponseEntity.status(status).body(reponse);
					}

				}
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized");

	}

	/**
	 * controlleur post pour filtrer les salles disponibles selon les critères de
	 * reservation
	 * 
	 * @param mv           : ModelAndView
	 * @param session      : la session
	 * @param dateDebut    : la date de début de la réservation
	 * @param dateFin      : la date de fin de la réservation
	 * @param nom          : le nom de la réservation
	 * @param capacite     : la capacité minimum de la salle à réserver
	 * @param tabTypeSalle : les types de salle souhaités
	 * @param params       : la liste des parametres (pour récupérer la liste de
	 *                     materiel et la quantité)
	 * @return
	 */
	@PostMapping(value = "/salledispo/{page}")
	public ResponseEntity<?> reservationSallePost(@RequestBody Reservation reservation,
			@RequestHeader HttpHeaders header,@PathVariable int page) {

		boolean ok = true;
		Object reponse;
		Map<String, Object> map = new HashMap<String, Object>();
		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));

		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));

			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), true)) {

				if (!ControleSaisie.isDateReservation("" + reservation.getDateDebut(), "" + reservation.getDateFin())) {
					ok = false;
					map.put("dateKO", "* Dates invalides");
				}

				if (!ControleSaisie.isNonVide(reservation.getNomReservation())) {
					ok = false;
					map.put("nomKO", "*veuillez entrer un nom de réservation");
				}

				if (ok) {
					List<Salle> listeSalle = servSalle.getAllSalles();

					// filtre salle actif
					listeSalle = servFiltreSalle.filtreActif(listeSalle, true);

					// filtre date salle dispo
					listeSalle = servFiltreSalle.filtreDateSalle(listeSalle, reservation.getDateDebut(),
							reservation.getDateFin(), true);

					listeSalle = listeSalle.stream().sorted((s1,s2)-> s1.getNumero() - s2.getNumero() ).collect(Collectors.toList());
					
					
					
					if(listeSalle.isEmpty()) {
						map.put("aucuneSalle", "désolé, il n'y a das de salle disponible");
						status = HttpStatus.BAD_REQUEST;
						reponse = map;
						return ResponseEntity.status(status).body(map);
						
					}else {
						int nombreTotalSalle = listeSalle.size();
						int nombrePage = (nombreTotalSalle / Parametrage.NB_SALLE_PAGE)
								+ (nombreTotalSalle % Parametrage.NB_SALLE_PAGE != 0 ? 1 : 0);
						
						if(page>nombrePage) {
							page=nombrePage;
						}
						if(page<1) {
							page=1;
						}
						
						int debut = page * Parametrage.NB_SALLE_PAGE - Parametrage.NB_SALLE_PAGE;
						int fin = page * Parametrage.NB_SALLE_PAGE;
						
						
						
						if (fin > listeSalle.size()) {
							fin = listeSalle.size();
						}
					listeSalle = listeSalle.subList(debut, fin);
						map.put("liste", listeSalle);
						map.put("nbPage", nombrePage);

						status = HttpStatus.OK;
						return ResponseEntity.status(status).body(map);
					}
				} else {
					status = HttpStatus.BAD_REQUEST;
					reponse = map;

				}
				return (ResponseEntity<?>) ResponseEntity.status(status).body(reponse);
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized");

	}

	/**
	 * Controlleur pour supprimer une reservation
	 * 
	 * @param mv      : ModelAndView
	 * @param session : la session
	 * @param id      : id de la reservation
	 * @return : ModelAndView correspondant au récapitulatif de la réservation
	 */
	@DeleteMapping(value = "/reservation/{id}")
	public ResponseEntity<?> deleteReservation(@PathVariable("id") String id, @RequestHeader HttpHeaders header) {

		
		Map<String, String> map = new HashMap<String, String>();
		HttpStatus status;
		final Optional<List<String>> headerAutorisation = Optional.ofNullable(header.get(HttpHeaders.AUTHORIZATION));

		if (headerAutorisation.isPresent()) {
			final Optional<String> token = Optional.ofNullable(headerAutorisation.get().get(0));

			if (token.isPresent() && JwtUtils.isAuthentificate(token.get(), true)) {

				if (ControleSaisie.isIntegerPositif(id)) {
					Reservation reservation = servReservation.getById(Integer.parseInt(id));
					if (reservation == null) {
						map.put("message", "la réservation n'existe pas ");
					} else if (reservation.getDateDebut().isAfter(LocalDate.now())) {
						Reservation reservSuppr = servReservation.deleteById(Integer.parseInt(id));
						if(reservSuppr!=null) {
							status = HttpStatus.OK;
						map.put("message", "la reservation a été supprimé");
						return ResponseEntity.status(status).body(map);
						}else {
							status = HttpStatus.INTERNAL_SERVER_ERROR;
							map.put("message", "erreur serveur la reservation n'a pas été supprimé");
							return ResponseEntity.status(status).body(map);
						}
					} else {
						status = HttpStatus.BAD_REQUEST;
						map.put("message", "une reservation en cours ne peux pas être supprimé");
						return ResponseEntity.status(status).body(map);
					}
					
				}
			}
		}
		status = HttpStatus.UNAUTHORIZED;
		return ResponseEntity.status(status).body("unauthorized");

	}

}
