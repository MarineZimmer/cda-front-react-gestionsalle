package fr.afpa.salleafpa.dao.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.salleafpa.dao.entities.SalleDao;

@Repository
public interface ServiceSalleRepository extends JpaRepository<SalleDao, Integer> {

}
