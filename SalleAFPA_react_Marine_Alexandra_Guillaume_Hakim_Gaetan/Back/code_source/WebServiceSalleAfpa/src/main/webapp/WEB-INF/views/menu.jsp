<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="fr.afpa.salleafpa.outils.Parametrage"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

			<nav>
				<ul>
					<li><a href="${ Parametrage.URI_DECONNEXION}">Se
							deconnecter</a></li>
					<li><a 
						href="${ Parametrage.URI_VISUALISER_SALLE}"> Visualisation
							salle </a></li>
					<c:if
						test="${sessionScope.persAuthSalle.role.id eq Parametrage.ADMIN_ID }">
						</li>
						<li><a href="${ Parametrage.URI_RESERVER_SALLE}">
								Reserver salle </a></li>
						<li><a href="${ Parametrage.URI_AJOUTER_SALLE}"> Ajouter
								salle </a></li>
						
								<li><a href="${ Parametrage.URI_AJOUTER_BATIMENT}"> Ajouter bâtiment</a></li>
								<li><a href="${ Parametrage.URI_LISTE_RESERVATION}/1"> Liste des réservations</a></li>
					</c:if>
				</ul>
			</nav>
			