<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
  <%@page import="fr.afpa.salleafpa.outils.Parametrage"%>
<%@page import="org.omg.CORBA.portable.ApplicationException"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Cr�ation Salle</title>
<link href="${pageContext.request.contextPath}/resources/css/menu.css" rel="stylesheet">
<link href ="${pageContext.request.contextPath}/resources/css/cssCreationSalle.css" rel="stylesheet">
</head>
<body>
	<c:choose>
		<c:when
			test="${sessionScope.persAuthSalle.role.id eq Parametrage.ADMIN_ID }">
			
			<jsp:include page="menu.jsp" />

        <div class="container">
            
                <h2>Creation d'une nouvelle salle</h2>         
            <div class="ligne">
            
            <!--Debut de FORM --> 
                <form action = "${Parametrage.URI_AJOUTER_SALLE}"method="post">
                
                    <!--COLONNE GAUCHE-->
                    <div class="colonne">
                    
                        <!--partie nom-->
                        <div class="colonneGauche"> 
                            <label>Nom :</label>
                        </div>
                        <div class="colonneDroite"> 
                            <input name="nom" value="${nom}" type="text" required="">
                             <br>
                            <em>${nomKO }</em>
                        </div>
                        
                        <!--partie numero-->
                        <div class="colonneGauche">
                            <label>Numero :</label>
                        </div>
                        <div class="colonneDroite"> 
                            <input name="numero" value="${numero}" type="number" required="">
                             <br>
                            <em>${numeroKO}</em>
                        </div>
                        
                        <!--partie etage-->
                        <div class="colonneGauche">
                            <label>Etage :</label>
                        </div>
                        <div class="colonneDroite"> 
                            <input name="etage" value="${etage}" type="number" required="">
                            
                             <br>
                            <em>${etageKO}</em>
                        </div>
                        
                        <!--partie capacite-->
                        <div class="colonneGauche">
                            <label>Capacit� :</label>
                        </div>
                        <div class="colonneDroite"> 
                            <input name="capacite" value="${capacite}" type="number" required="">
                            
                             <br>
                            <em>${capaciteKO} </em>
                        </div>
                        
                        <!--partie adresse-->
                        <div class="colonneGauche">
                            <label>Superficie :</label>
                        </div>
                        <div class="colonneDroite"> 
                            <input name="superficie" value="${superficie}" type="number" step="0.1" required="">
                          
                              <br>
                            <em>${superficieKO}</em>
                             
                        </div>
                        
                        
                        <!--partie date-->
                        <div class="colonneGauche">
                           
                        </div>
                        <div class="colonneDroite"> 
                           
                        </div>
                    </div>
                    
                    
                    <!--COLONNE DROITE-->
                    <div class="colonne">
                    
                        <!--partie batiment-->
                        <div class="colonneGauche"> 
                            <label>Batiment :</label>
                        </div>
                        <div class="colonneDroite"> 
                        
                           <select name="batiment" id="selectBatiment" required="">
                                <option value="">--Veuillez choisir un batiment--</option>
                                <c:forEach var="batiment" items="${listeBatiment }">
                              <option value="${batiment.id }">${batiment.nom }</option>
                              </c:forEach>
                            </select>
                                
                        </div>
                        
                        <!--partie type salle-->
                        <div class="colonneGauche"> 
                            <label>Type de salle :</label>
                        </div>
                        <div class="colonneDroite"> 
                          <select name="typeSalle" id="selectTypeSalle" required="">
                                <option value="">--Veuillez choisir le type de salle--</option>
                                <c:forEach var="typeSalle" items="${listeTypeSalle }">
                              <option value="${typeSalle.id }">${typeSalle.libelle }</option>
                              </c:forEach>
                            </select>
                        </div>
                        
                        <!--partie type materiel-->
                        <c:forEach var= "typeMateriel" items="${listeTypeMateriel }">
                       
                        <div class="colonneGauche"> 
                            <label>${typeMateriel.libelle } :</label>
                        </div>
                        
                       
                        <div class="colonneDroite"> 
                        
                             <input id ="${typeMateriel.idTypeMateriel}" name="tm${typeMateriel.idTypeMateriel}" value="" type="number" size="3" required="">
                         
                        </div>
                        </c:forEach>
                        
                        <!--partie Fonction a voir comment integrer d'autres fonction via java -->
                        
                        <div class="colonneGauche"> 
                         
                        </div>
                        <div class="colonneDroite"> 
                        
                                            
                        </div>
                       
                      
                       <!--colonnes vides pour le positionnement-->
                          <div class="colonneGauche"> 
                            <label></label>
                        </div>
                        <div class="colonneDroite"> 
                            <label></label>
                        </div>
                        
                        <!--partie BOUTTON VALIDER-->
                        <div class="colonneGauche"> 
                            <label></label>
                        </div>
                        <div class="colonneDroite"> 
                            <button type="submit" value="Valider">Valider</button>
                        </div>
                        
                        
                        <!--partie BOUTTON ANNULER retour vers l'accueil � integrer-->
                        <div class="colonneGauche"> 
                            <label></label>
                        </div>
                        <div class="colonneDroite"> 
                            <button type="reset" value="Annuler">Annuler</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
   
		</c:when>
		<c:otherwise>
			<c:redirect url=""></c:redirect>
		</c:otherwise>
	</c:choose>
</body>
</html>