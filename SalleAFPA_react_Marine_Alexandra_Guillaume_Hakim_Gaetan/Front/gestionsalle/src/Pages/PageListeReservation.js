import React, { useState, useEffect,Fragment } from 'react';
import Header from '../Components/Header'
import TableauReservation from '../Components/TableauReservation'
import {  useToasts } from 'react-toast-notifications';
import {useParams,useHistory} from 'react-router-dom'

export default function ListeReservation() {

    const [reservations, setReservations] = useState([]);
    const { addToast } = useToasts();
    const [show, setShow] = useState(false)
    const { page } = useParams();
    const [refresh, setRefresh] = useState(false);
    const history = useHistory();
   
        const getReservations =  async () => {
            let pageUrl = page ? page : 1;       
            let response = await fetch(`${process.env.REACT_APP_API_URL}/reservationpage/${pageUrl}`, {
                method: "GET",
                headers: {
                    'Accept': 'application/json',
                    "Content-type": "application/json; charset=UTF-8",
                    'Authorization': sessionStorage.token
                }
            })
            .catch(error => {console.log(error);}); 
            if (response && response.status===200) {
                let responseData = await response.json();
                setReservations(responseData);
                setShow(true)
            }else if(response && response.status===401){
                addToast("authentification requise", { appearance: 'error',autoDismiss: true,autoDismissTimeout : 1000  , onDismiss:(id)=>{history.push('/')}})
               
              } else {
                addToast("erreur serveur", { appearance: 'error',autoDismiss: true  })
            }
            
    }

    
    useEffect(() => {
        getReservations();
    }, [refresh,page]);



    return <Fragment>
        <Header />
    {!show ? (
      <div></div>
    ) : ( <div className="tabReservation">
                <TableauReservation reservations={reservations.listeReservation} nbPage={reservations.nbPage} page={page ? page : 1} setRefresh={setRefresh} refresh={refresh}/>
       </div>)
    }</Fragment>
}