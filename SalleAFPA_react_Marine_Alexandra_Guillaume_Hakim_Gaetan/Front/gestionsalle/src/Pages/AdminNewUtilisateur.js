import React from 'react';
import Header from '../Components/HeaderAdmin'
import NewUser from '../Components/FormCreationUser'
export default function PageCreationSalle(){

    return <div>
        <Header/>
        <NewUser/>
    </div>
}