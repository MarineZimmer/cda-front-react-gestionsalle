import React, { Fragment } from 'react';
import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';
import {useHistory} from 'react-router-dom';


export default function ModalSalle({ typeEvent, salle, show, setShow }) {
    const creationSalle = (typeEvent === 2);
    const history = useHistory();
    function toggle(reponse) {
        setShow(!show);
        if (reponse) {
            history.push('/listesalle');
        }
    }


    return (<Fragment>
        {!show ? (
            <MDBModal isOpen={show} toggle={toggle}></MDBModal>
        ) : (
                <MDBModal isOpen={show} toggle={toggle} centered>
                    <MDBModalHeader toggle={toggle} >
                        {!creationSalle ? (
                            <span> Création de la Salle  Reussi</span>
                        ) :
                            (<span> Modification de la Salle  Reussi</span>)}
                    </MDBModalHeader>
                    <MDBModalBody>
                    {!creationSalle ? (
                            <span> Vous venez de créer la salle </span>
                        ) :
                            (<span> Vous venez de modifier la salle </span>)}
                        {salle.nom}, n° {salle.numero}  <br /><br />
                        Type de Salle : {salle.typeSalle.libelle}<br /><br />
                        Etage de la salle : {salle.etage}<br /> <br />
                        Capacité de la salle : {salle.capacite} personne(s)<br /><br />
                        Superficie de la salle : {salle.surface} m²
                    </MDBModalBody>
                    <MDBModalFooter>

                        <MDBBtn className="btnGeneralDark" onClick={() => toggle(true)} >
                            OK
                            </MDBBtn>

                    </MDBModalFooter>
                </MDBModal>
            )}</Fragment>
    );

}
