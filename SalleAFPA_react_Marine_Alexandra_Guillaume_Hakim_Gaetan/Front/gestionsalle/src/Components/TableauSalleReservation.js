import React, { Fragment } from 'react';
import Salle from './SalleReservation';

export default function TableauSalle({salles,reservation,setReservation,page,setPage,nbPage,setLoad}) {
    

    const salleListe = salles.map((salle) => <Salle key={salle.id} salle={salle}  reservation={reservation}  setReservation={setReservation} setLoad={setLoad} />);

    const pagination = () =>{
        let retour=[];
        for(let i=1;i<=nbPage;i++){
            retour.push(<li className={"page-item " + (i==page ? 'active' : '')} ><a className="page-link" onClick={() => {modifPage(i)}} >{i}</a></li>)
        }
        return retour;
    }


    function modifPage(nb){
        setPage(nb);
    }
    return <Fragment><div class="listeSalle">
        
        { salleListe.length === 0 ? (<div></div>) : 
        (<Fragment><h3 >Liste des salles disponibles :</h3> 
        <div className="row">
        {salleListe}</div>
        <nav aria-label="...">
            <ul className="pagination justify-content-center">
                <li className={"page-item " + ((page-1)<1 ? 'disabled' : '')}>
                <a className="page-link" onClick={() => {modifPage(page-1)}} >&laquo;</a>
                </li> 
                {pagination()}
                <li className={"page-item " + ((parseInt(page)+1)>nbPage ? 'disabled' : '')}>
                    <a className="page-link" onClick={() => {modifPage(page+1)}}>&raquo;</a>
                </li>
            </ul>
        </nav></Fragment>) }
       
        </div> </Fragment>;
}