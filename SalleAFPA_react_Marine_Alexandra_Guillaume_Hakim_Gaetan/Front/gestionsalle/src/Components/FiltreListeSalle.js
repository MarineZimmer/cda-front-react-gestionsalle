import React, { useState, useEffect } from 'react';
import { useToasts } from 'react-toast-notifications';
import {useHistory} from 'react-router-dom';

export default function FiltreSalle({ filtres, filtrer }) {
  const [typeSalles, setTypesSalles] = useState([]);
  const [typeMateriels, setTypesMateriels] = useState([]);
  const [batiments, setBatiments] = useState([]);
  const { addToast } = useToasts();
  const history = useHistory();

  const getTypeSalles = async () => {
    let response = await fetch(`${process.env.REACT_APP_API_URL}/typesalle`, {
      headers: {
        'Authorization': sessionStorage.token
      }
    })
      .catch(error => console.log(error))

    if (response && response.status === 200) {
      let responseData = await response.json();
      setTypesSalles(responseData);
    } else if (response && response.status === 401) {
      addToast("authentification requise", { appearance: 'error', autoDismiss: true, autoDismissTimeout: 1000, onDismiss: (id) => {  history.push('/'); } })

    } else {
      addToast("erreur serveur", { appearance: 'error', autoDismiss: true })
    }
  }


  const getTypeMateriels = async () => {
    let response = await fetch(`${process.env.REACT_APP_API_URL}/typemateriel`, {
      headers: {
        'Authorization': sessionStorage.token
      }
    })
      .catch(error => console.log(error))

    if (response && response.status === 200) {
      let responseData = await response.json();
      setTypesMateriels(responseData);
    } else if (response && response.status === 401) {
      addToast("authentification requise", { appearance: 'error', autoDismiss: true, autoDismissTimeout: 1000, onDismiss: (id) => {  history.push('/')  } })

    } else {
      addToast("erreur serveur", { appearance: 'error', autoDismiss: true })
    }
  }


  const getBatiments = async () => {
    let response = await fetch(`${process.env.REACT_APP_API_URL}/batiment`, {
      headers: {
        'Authorization': sessionStorage.token
      }
    })
      .catch(error => console.log(error))

    if (response && response.status === 200) {
      let responseData = await response.json();
      setBatiments(responseData);
    } else if (response && response.status === 401) {
      addToast("authentification requise", { appearance: 'error', autoDismiss: true, autoDismissTimeout: 1000, onDismiss: (id) => {  history.push('/')} })

    } else {
      addToast("erreur serveur", { appearance: 'error', autoDismiss: true })
    }
  }

  useEffect(() => {
    getTypeSalles();
    getTypeMateriels();
    getBatiments();
  }, []);


  function changeHandler(event) {
    if (event.target.name === "typeSalle") {
      if (filtres.tabTypeSalle.includes(event.target.value)) {
        filtres.tabTypeSalle = filtres.tabTypeSalle.filter(elt => elt !== event.target.value)
      } else {
        filtres.tabTypeSalle.push(event.target.value)
      }
    } else if (event.target.name === "actif") {
      filtres.actif = event.target.value;
    } else if (event.target.name === "capacite") {
      filtres.capacite = event.target.value;
    } else if (event.target.name === "batiment") {
      if (filtres.batiment.includes(event.target.value)) {
        filtres.batiment = filtres.batiment.filter(elt => elt !== event.target.value)
      } else {
        filtres.batiment.push(event.target.value)
      }
    } else if (event.target.name === "date") {
      filtres.date = event.target.value;
    } else {
      filtres[event.target.name] = event.target.value;
    }
    filtrer();
  };


  function submit(event) {
    event.preventDefault();
    filtrer()
  }

  const filtreTypeSalle = typeSalles.map(typeSalle => <div className="form-check">
    <input id="chkTS" className="form-check-input" type="checkbox" value={typeSalle.id} name="typeSalle" onChange={changeHandler} />
    <label className="form-check-label" for="chkTS"> {typeSalle.libelle}</label></div>)

  const filtreBatiment = batiments.map(batiment => <div className="form-check">
    <input id="chkBat" className="form-check-input" type="checkbox" value={batiment.id} name="batiment" onChange={changeHandler} />
    <label className="form-check-label" for="chkBat"> {batiment.libelle}</label></div>)

  const filtreTypeMateriel = typeMateriels.map(typeMateriel =>
    <div className="container row justify-content-between">
      <div className="form-group">
        <label for="inptNb">{typeMateriel.libelle} :</label>
        <input type="number" class="form-control inputsFiltres" id="inptNb" name={"tm" + typeMateriel.id} onChange={changeHandler} />
      </div>
    </div>)


  const filtreActif = <div>
    <div className="form-check">
      <input className="form-check-input" name="actif" value="true" type="radio" id="act" onChange={changeHandler} />
      <label className="form-check-label" for="act"> Active</label>
    </div>

    <div className="form-check">
      <input className="form-check-input" name="actif" value="false" type="radio" id="desact" onChange={changeHandler} />
      <label className="form-check-label" for="desact"> Non active      </label>
    </div>
    <div class="form-check">
      <input className="form-check-input" name="actif" value="" type="radio" id="tt" onChange={changeHandler} />
      <label className="form-check-label" for="tt">Toutes les salles</label><br />
    </div>
  </div>


  return <div>
    <button className="btn btnFiltres" type="button" data-toggle="collapse" data-target="#collapseFiltre" aria-expanded="false" aria-controls="collapseExample">
      Filtrer les salles <i className="fas fa-sort-down" />
    </button>

    <div className="collapse filtre" id="collapseFiltre">
      <form onSubmit={submit} >



        <div className="input-group">
          <div className="input-group-prepend">
            <span className="input-group-text">Nom</span>
          </div>
          <input type="text" name="nom" className="inputsFiltres" id="inputNomFiltre"onChange={changeHandler} />
        </div>
        <br />
        <hr />


        <div className="form-group">
          <h5>Filtrer Salle :</h5>
          {filtreActif}
        </div>
        <hr />
        <div className="form-group">
          <h5>Filtrer par Bâtiment :</h5>
          {filtreBatiment}
        </div>
        <hr />
        <div className="form-group">
          <h5>Filtrer par Type de Salle :</h5>
          {filtreTypeSalle}
        </div>
        <hr />

        <div className="form-group">
          <h5>Filtrer par Capacité :</h5>
          <div className="container row justify-content-between">
            <label for="inptCap">Capacité :</label>
            <input id="inptCap" className="form-control inputsFiltres" type="number" name="capacite" onChange={changeHandler} />
          </div>
        </div>


        <hr />
        <div className="form-group">
          <strong>Filtrer par Disponibilité :</strong>
          <div>
            <div className="form-check">
              <input id="cbReserve" className="form-check-input" name="date" value="reserve" type="radio" onChange={changeHandler} />
              <label className="form-check-label" for="cbReserve">Reservées</label>
            </div>
            <div className="form-check">
              <input id="cblibre" className="form-check-input" name="date" value="libre" type="radio" onChange={changeHandler} />
              <label className="form-check-label" for="cblibre">Libres      </label>
            </div>
            <div className="form-check">
              <input id="cblibre" className="form-check-input" name="date" value="nonFiltre" type="radio" onChange={changeHandler} />
              <label className="form-check-label" for="cblibre">Toutes</label>
            </div>

            <br />
          </div>

          <div className="dateDebutFin">

            <div className="input-group">
              <div className="input-group-prepend">
                <span className="input-group-text">Date de début :</span>
              </div>
              <input id="dateDebut" className="inputsFiltres" name="dateDebut" type="date" defaultValue={new Date().toISOString().slice(0, 10)} onChange={changeHandler} />
            </div>

            <div className="input-group">
              <div className="input-group-prepend">
                <span className="input-group-text">Date de fin :</span>
              </div>
              <input id="dateFin" className="inputsFiltres" name="dateFin" type="date" defaultValue={new Date().toISOString().slice(0, 10)} onChange={changeHandler} />
            </div>
          </div>
        </div>
      </form>
    </div>

  </div>;
}