import React, { useEffect } from 'react';
import { MDBRow, MDBBtn, MDBInput } from "mdbreact";


export default function FormCreationReservation({rechercheSalle,reservation,setReservation,messagesErreur}) {

  function changeHandler(event) {
    let reservationTemp = reservation

    reservationTemp[event.target.name] = event.target.value;
    
    setReservation(reservationTemp)
  };

  async function submitForm(event) {
    event.preventDefault();
   rechercheSalle(reservation);

  };
 
  useEffect(()=>{

  },[reservation])

  return <div className="formReservation">
    
    <h4>Nouvelle Reservation : </h4>
    <form
      className="needs-validation"
      onSubmit={submitForm}
      noValidate
    >
     
      <MDBRow>
          <MDBInput
            value={reservation.nom}
            name="nomReservation"
            onChange={changeHandler}
            type="text"
            id="nom"
            label="Nom"
            required
          >
            <em  className="text-center red-text" >{messagesErreur.nomKO}</em>
          </MDBInput>
          </MDBRow>
          <MDBRow>
        <MDBInput
            valueDefault={reservation.dateDebut}
            name="dateDebut"
            onChange={changeHandler}
            type="date"
            id="dateDebut"
            label="Date de debut"
            required>
          </MDBInput></MDBRow>
          <MDBRow>
        <MDBInput
            valueDefault={reservation.dateFin}
            name="dateFin"
            onChange={changeHandler}
            type="date"
            id="dateFin"
            label="Date de fin"
            required>
               <em  className="text-center red-text" >{messagesErreur.dateKO}</em>
          </MDBInput></MDBRow>
         <MDBRow><MDBBtn className="btnGeneralBlanc" type="submit">Afficher les salles disponibles</MDBBtn></MDBRow>
    </form> 
  </div>
}