import React, { Fragment } from 'react';
import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';

export default function ModalConfirmationReservation({ reservation, show, setShow, reserver, salle }) {

    function toggle(reponse) {
        setShow(!show);
        if (reponse) {
            reserver();
        }
    }
    return (<Fragment>
        {!show ? (
            <MDBModal isOpen={show} toggle={toggle}></MDBModal>
        ) : (
                <MDBModal isOpen={show} toggle={toggle}>
                    <MDBModalHeader toggle={toggle}>
                        Confirmation Réservation
        </MDBModalHeader>
                    <MDBModalBody>
                        Nom de la réservation : {reservation.nomReservation}<br />
                        Salle {salle.nom} n° {salle.numero}  <br/>

                        date de début : {new Date(reservation.dateDebut).toLocaleDateString('fr-FR')}<br />
                        date de fin : {new Date(reservation.dateFin).toLocaleDateString('fr-FR')}<br />
                    </MDBModalBody>
                    <MDBModalFooter>
                        <MDBBtn className="btnGeneralDark" onClick={() => toggle(true)}>
                            OUI
                        </MDBBtn>
                        <MDBBtn className="btnGeneralBlanc" onClick={() => toggle(false)}>
                            NON
                        </MDBBtn>
                    </MDBModalFooter>
                </MDBModal>
            )}</Fragment>);
}