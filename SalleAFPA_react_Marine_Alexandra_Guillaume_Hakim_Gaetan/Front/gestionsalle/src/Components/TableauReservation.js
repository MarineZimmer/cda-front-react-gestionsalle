import React, { useState }  from 'react';
import {Link,useHistory} from 'react-router-dom';
import LigneTableauReservation from './LigneTableauReservation';

import { useToasts } from 'react-toast-notifications'
import ModalSuppr from './ModalSupprReservation';

import ModalModif from './ModalModifReservation';

import ModalDetails from './ModalDetailsReservation';

export default function TableauReservations({ reservations ,nbPage,page,refresh,setRefresh}) {

    const[reservation,setReservation]=useState({});
    const[show,setShow]=useState(false);
    const[showModif,setShowModif]=useState(false);
    const[messagesErreur,setMessagesErreur]=useState([]);
    const { addToast } = useToasts();

    const[showDetails,setShowDetails]=useState(false);
    const history = useHistory();
    async function suppr() {
     
          let response = await fetch(`${process.env.REACT_APP_API_URL}/reservation/` + reservation.id, {
              method: "DELETE",
              headers: {
                  'Authorization': sessionStorage.token
              }
          })
              .catch(error => { console.log(error); });
              if (response && response.status === 200) {
                  addToast("suppression effectuée", { appearance: 'success', autoDismiss: true })
                  setRefresh(!refresh)
                }else if(response && response.status===401){
                  addToast("authentification requise", { appearance: 'error' })
                  history.push('/')
                } else if(response && response.status===400) {
                    let responseData = await response.json();
                  addToast("erreur suppression : "+responseData.message , { appearance: 'error', autoDismiss: true })
                }
                else  {
                  addToast("erreur serveur", { appearance: 'error', autoDismiss: true })
                }
      }


      async function modification(reservationModif) {
     
        let response = await fetch(`${process.env.REACT_APP_API_URL}/reservation`, {
            method: "PUT",
            body: JSON.stringify(reservationModif),
      headers: {
        'Accept': 'application/json',
        "Content-type": "application/json; charset=UTF-8",
        'Authorization': sessionStorage.token
      }
        })
            .catch(error => { console.log(error); });
            if (response && response.status === 200) {
                addToast("modification effectuée", { appearance: 'success', autoDismiss: true })
                setShowModif(false);
                setMessagesErreur([]);
                setRefresh(!refresh)
              }else if(response && response.status===401){
                addToast("authentification requise", { appearance: 'error' })
                history.push('/')
              } else if(response && response.status===400) {
                  let responseData = await response.json();
                  setMessagesErreur(responseData);
                addToast("erreur modification"  , { appearance: 'error', autoDismiss: true })
                
              }
              else  {
                addToast("erreur serveur", { appearance: 'error', autoDismiss: true })
              }
              return false;
    }
  
    
    const reservationsList = reservations.map((reservation) => <LigneTableauReservation key={reservation.id} reservation={reservation} setShowDetails={setShowDetails} setShow={setShow} setShowModif={setShowModif} setReservation={setReservation}/>);
    const pagination = () =>{
    let retour=[];
    for(let i=1;i<=nbPage;i++){
        retour.push(<li className={"page-item " + (i==page ? 'active' : '')} ><a className="page-link" onClick={ ()=>history.push("/listereservaton/"+i)}>{i}</a></li>)
    }
    return retour;
}
    return <div className="container">

        <h3 className="text-center">Liste des réservations :</h3>
        <hr/>
        <div class="table-responsive">
        <table className="table  table-striped table-hover ">
            <thead class="thead-dark text-center">
                <tr >
                    <th className="thReservationFont">Nom</th>
                    <th className="thReservationFont" scope="col">Date début</th>
                    <th className="thReservationFont" scope="col">Date fin</th>
                    <th className="thReservationFont"  scope="col">Nom de la salle</th>
                    <th className="thReservationFont" scope="col">Numero de la salle</th>
                    <th className="thReservationFont" scope="col">Bâtiment</th>
                    <th  colspan="2"><i className="fas fa-cogs "/></th>
                   
                </tr>
            </thead>
            <tbody>
                {reservationsList}
            </tbody>
        </table>
        </div>
        <nav aria-label="...">
            <ul className="pagination justify-content-center">
                <li className={"page-item " + ((page-1)<1 ? 'disabled' : '')}>
                <Link className="page-link" to={"/listereservaton/"+(page-1)} >&laquo;</Link>
                </li> 
                {pagination()}
                <li className={"page-item " + ((parseInt(page)+1)>nbPage ? 'disabled' : '')}>
                    <Link className="page-link" to={"/listereservaton/"+(parseInt(page)+1)}>&raquo;</Link>
                </li>
            </ul>
        </nav>
        <ModalSuppr reservation={reservation} show={show} setShow={setShow} suppr={suppr}/>
        <ModalModif  setMessagesErreur={setMessagesErreur} reservation={reservation} show={showModif} setShow={setShowModif} messagesErreur={messagesErreur} modification={modification}/>
        <ModalDetails  reservation={reservation} show={showDetails} setShow={setShowDetails}/>
    </div>;
}