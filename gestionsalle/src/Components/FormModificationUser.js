import React, { useState, useEffect, Fragment } from 'react';
import { MDBRow, MDBCol, MDBBtn, MDBInput } from "mdbreact";
import {useParams, useHistory} from 'react-router-dom'
import ModalUser from './ModalUser'
import { useToasts } from 'react-toast-notifications';

export default function FormModificationUser() {
  const [user, setUser] = useState({});
  const [fonctions, setFonctions] = useState([]);
  const [roles, setRoles] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [reponseServeur, setReponseServeur] = useState({});
  const [show, setShow] = useState(false)
  const history = useHistory();
  
  const { login } = useParams();
  const { addToast } = useToasts();
  const [erreur, setErreur] = useState(false)


  const getFonctions = async () => {
    let response = await  fetch(`${process.env.REACT_APP_API_URL}/fonction`, {
       headers: {
           'Authorization': sessionStorage.token
       }
   }).catch(error => console.log(error))
   if (response && response.status===200) {
     let responseData = await response.json();
     setFonctions(responseData);
 }else if(response && response.status===401){
  setErreur(true)
   } else {
    setErreur(true)
 }
   }
 

  const getRoles = async () => {
    let response =  await fetch(`${process.env.REACT_APP_API_URL}/role`, {
      headers: {
          'Authorization': sessionStorage.token
      }
  })
      .catch(error => console.log(error))
      if (response && response.status===200) {
        let responseData = await response.json();
        setRoles(responseData);
    }else if(response && response.status===401){
   
       setErreur(true)
      } else {
        setErreur(true)
      
    }
  }

  const getUser = async () => {
   let response =  await fetch(`${process.env.REACT_APP_API_URL}/personne/${login}`, {
      headers: {
          'Authorization': sessionStorage.token
      }
  }).catch(error => console.log(error))

      if(response && response.status===200) {
        let reponseJson  = await response.json();
        setUser(reponseJson)
      }else if(response && response.status===400) {
        let reponseJson  = await response.json();
        setReponseServeur(reponseJson)
        addToast("personne non existante", { appearance: 'error',autoDismiss: true  })
      }else if(response && response.status===401){
        setErreur(true)
        addToast("authentification requise", { appearance: 'error',autoDismiss: true,autoDismissTimeout : 1000  , onDismiss:(id)=>{history.push("/");}})
      }else{
        setErreur(true)
        addToast("erreur serveur", { appearance: 'error',autoDismiss: true  })
      }
  }

  function changeHandler(event) {
    let userTemp = user
    if (event.target.name === "fonction") {
      userTemp.fonction = fonctions.find(e => e.id == event.target.value)
    } else if (event.target.name === "role") {
      userTemp.role = roles.find(e => e.id == event.target.value)
    } else if (event.target.name === "mdp") {
      userTemp.authentification.mdp = event.target.value
    } else if (event.target.name === "login") {
      userTemp.authentification.login = event.target.value
    } else {
      userTemp[event.target.name] = event.target.value;
    }
    setUser(userTemp)
  };



  async function submitForm(event) {
    event.preventDefault();
    let response = await fetch(`${process.env.REACT_APP_API_URL}/personne`, {
      method: "PUT",
      body: JSON.stringify(user),
      headers: {
        'Accept': 'application/json',
        "Content-type": "application/json; charset=UTF-8",
        'Authorization': sessionStorage.token
      }
    })
    .catch(error => console.log(error));

    if(response && response.status===200) {
      let reponseJson  = await response.json();
      setReponseServeur(reponseJson)
      addToast("la modification a été effectuée", { appearance: 'success',autoDismiss: true })
      setShow(true)
    }else if(response && response.status===400) {
      let reponseJson  = await response.json();
      setReponseServeur(reponseJson)
      addToast("la modification a échouée", { appearance: 'error',autoDismiss: true  })
    }else if(response && response.status===401){
      addToast("authentification requise", { appearance: 'error',autoDismiss: true,autoDismissTimeout : 1000  , onDismiss:(id)=>{history.push("/");}})
    }else{
      addToast("erreur serveur", { appearance: 'error',autoDismiss: true  })
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      await getUser();
      await getFonctions();
      await getRoles();
      setIsLoading(false);
    }
    fetchData()
   
  }, []);

  const fonctionsList = fonctions.map((fonction) => <option key={fonction.id} value={fonction.id} selected={fonction.id===user.fonction.id} >{fonction.libelle}</option>);
  const rolesList = roles.map((role) => <option key={role.id} value={role.id} selected={role.id===user.role.id} >{role.libelle}</option>);
 
  return <Fragment>
    {isLoading || erreur ? (
      <div></div>
    ) : (<div className="user">
      <h3>Formulaire de modification de l'Utilisateur : </h3>
      <hr/>
      <form
        className="needs-validation formUser"
        onSubmit={submitForm}
        noValidate
      >
        <MDBRow className="d-flex justify-content-center">
          <MDBCol md="4" className="mb-3">
            <MDBInput
              valueDefault={user.nom}
              name="nom"
              onChange={changeHandler}
              type="text"
              id="nom"
              label="Nom"
              required
            >
              <em  className="text-center red-text" >{reponseServeur.nomKO}</em>
            </MDBInput>
          </MDBCol>
          <MDBCol md="4" className="mb-3">
            <MDBInput
              valueDefault={user.prenom}
              name="prenom"
              onChange={changeHandler}
              type="text"
              id="prenom"
              label="Prenom"
              required >
              <em  className="text-center red-text" >{reponseServeur.prenomKO}</em>
            </MDBInput>
          </MDBCol>
        </MDBRow>
        <MDBRow className="d-flex justify-content-center">
          <MDBCol md="4" className="mb-3">
            <MDBInput
              valueDefault={user.mail}
              name="mail"
              onChange={changeHandler}
              type="text"
              id="mail"
              label="Email"
              required>
              <em  className="text-center red-text" >{reponseServeur.mailKO}</em>
            </MDBInput>
          </MDBCol >
          <MDBCol md="4" className="mb-3">
            <MDBInput
              valueDefault={user.tel}
              name="tel"
              onChange={changeHandler}
              type="tel"
              id="tel"
              label="Telephone"
              required >
              <em  className="text-center red-text" >{reponseServeur.telKO}</em>
            </MDBInput>
          </MDBCol>
        </MDBRow>
        <MDBRow className="d-flex justify-content-center">
          <MDBCol md="8" className="mb-6">
            <MDBInput
              valueDefault={user.dateDeNaissance}
              name="dateDeNaissance"
              onChange={changeHandler}
              type="date"
              id="adresse"
              label="Date de naissance"
              required>
              <em  className="text-center red-text" >{reponseServeur.dateKO}</em>
            </MDBInput>
          </MDBCol>
        </MDBRow>
        <MDBRow className="d-flex justify-content-center">
          <MDBCol md="8" className="mb-6">
            <MDBInput
              valueDefault={user.adresse}
              name="adresse"
              onChange={changeHandler}
              type="text"
              id="adresse"
              label="Adresse"
              required>
              <em  className="text-center red-text" >{reponseServeur.adresseKO}</em>
            </MDBInput>
          </MDBCol>
        </MDBRow>
        <MDBRow className="d-flex justify-content-center">
          <MDBCol md="4" className="mb-3">
            <MDBInput
              valueDefault={user.authentification.login}
              name="login"
              onChange={changeHandler}
              type="text"
              id="login"
              label="Login"
              disabled
            >
              <em  className="text-center red-text" >{reponseServeur.loginKO}</em>
            </MDBInput>
          </MDBCol>
          <MDBCol md="4" className="mb-3">
            <MDBInput
              name="mdp"
              onChange={changeHandler}
              type="password"
              id="mdp"
              label="Mot de passe"
              required
            >
              <em  className="text-center red-text" >{reponseServeur.mdpKO}</em>
            </MDBInput>
          </MDBCol>
        </MDBRow>
        <MDBRow className="d-flex justify-content-center">
          <MDBCol md="4" className="mb-3">
            <label
              htmlFor="fonction"
              className="grey-text"
            >
              Fonction :
              </label>
            <select className="browser-default custom-select" name="fonction" onChange={changeHandler}>
              <option>Votre Fonction</option>
              {fonctionsList}
            </select>
            <em  className="text-center red-text" >{reponseServeur.fonctionKO}</em>
            <div className="valid-tooltip">Looks good!</div>
          </MDBCol>

          <MDBCol md="4" className="mb-3">
            <label htmlFor="role" className="grey-text">
              Role :
              </label>
            <select className="browser-default custom-select" name="role" onChange={changeHandler}>
              <option>Role</option>
              {rolesList}
            </select>
            <em  className="text-center red-text" >{reponseServeur.roleKO}</em>
          </MDBCol>
        </MDBRow>
        <MDBRow className="d-flex justify-content-center">
          <MDBCol md="4" className="mb-3" ></MDBCol>
          <MDBCol md="4" className="mb-3">
            <MDBBtn type="submit" className="btnModif">Valider</MDBBtn>
          </MDBCol>
        </MDBRow>
      </form>
      <ModalUser user={reponseServeur} show={show} setShow={setShow}/>
    </div>)}
  </Fragment>
}