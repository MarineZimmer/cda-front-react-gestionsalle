import React from 'react';
import {  useToasts } from 'react-toast-notifications';

import {useHistory} from 'react-router-dom';

export default function User({ user, suppression}) {
    const history = useHistory();

    const { addToast } = useToasts();



    function editable() {
       history.push("/admin/user/modif/"+ user.authentification.login);
    }


    async function supprimer() {
      if (window.confirm("voullez vous supprimer cet utilisateur ")) {
        let response = await fetch(`${process.env.REACT_APP_API_URL}/personne/` + user.authentification.login, {
            method: "DELETE",
            headers: {
                'Authorization': sessionStorage.token
            }
        })
            .catch(error => { console.log(error); });
   
            if (response && response.status===200) {
                addToast("utilisateur à été supprimé", { appearance: 'success',autoDismiss: true })
            }else if(response && response.status===400) {
                addToast("erreur : un administrateur ne peut pas être supprimé", { appearance: 'error',autoDismiss: true })
         
          }else if(response && response.status===401){
                addToast("authentification requise", { appearance: 'error',autoDismiss: true,autoDismissTimeout : 1000  , onDismiss:(id)=>{history.push("/");}})
              } else {
                addToast("erreur serveur", { appearance: 'error',autoDismiss: true  })
            }
        suppression();
    } 

    }

    


    return <tr className="trUser" d-table>
        <td className="text-center h6" >{user.authentification.login}</td>
        <td className="text-center h6">{user.nom}</td>
        <td className="text-center h6">{user.prenom}</td>
        <td className="text-center h6">{user.mail}</td>
        <td className="text-center h6">{user.tel}</td>
        <td className="text-center h6">{user.fonction.libelle}</td>
        <td className="text-center h6">{user.role.libelle}</td>
        <td className="text-center h6"><button className="btnEdit"  onClick={editable}><i className="fas fa-pencil-alt"></i></button></td>
        <td className="text-center h6"><button className="btnDelt"  onClick={supprimer}><i className="fas fa-trash-alt"></i></button></td>
        </tr>
}