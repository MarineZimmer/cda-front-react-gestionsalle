import React, { Fragment,useEffect }  from 'react';
import $ from 'jquery';
import { MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter ,MDBRow, MDBInput} from 'mdbreact';

export default function DetailsReserv({reservation, show, setShow}) {
 

    function toggle(reponse) {
        setShow(!show);

        }
    


    return (<Fragment>
        {!show ? (
            <MDBModal isOpen={show} toggle={toggle} ></MDBModal>
        ) : (
                <MDBModal isOpen={show} toggle={toggle} centered> 
                    <MDBModalHeader toggle={toggle} centered>
                      Informations de La Réservation :
                    </MDBModalHeader>
                    <MDBModalBody className="container" >
                    Salle réservée : {reservation.salle.nom}<br/> 
                    Type : {reservation.salle.typeSalle.libelle}<br/> 
                    Batiment : {reservation.salle.batiment.libelle}<br/> 
                    Numero : {reservation.salle.numero}<br/> 
                    Etage : {reservation.salle.etage}<br/> 
                    Capacité : {reservation.salle.capacite}<br/> <br/> 
                 
                    
                    </MDBModalBody>
                    <MDBModalFooter>
                       
                        <MDBBtn className="btnGeneralDark" onClick={()=>toggle(false)}>Retour</MDBBtn>
                    </MDBModalFooter>
                </MDBModal>
            )}</Fragment>);

}