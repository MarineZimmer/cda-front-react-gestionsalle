import React, { useState } from 'react';

import '../css/header.css'
import { Link } from 'react-router-dom';


export default function Header() {
    const [collapse, setCollapse] = useState(false);
    const [wideEnough, setWideEnough] = useState(false);



    function onClick() {
        setCollapse(!collapse);
    }

    function deconnexionOnClick() {
        sessionStorage.clear();
    }

    return (
        <nav className="navBarStyle navbar navbar-expand-lg">
        <div className="d-flex flex-grow-1">
            <span className="spanTitre w-100 d-lg-none d-block">{/*<!-- hidden spacer to center brand on mobile -->*/}</span>
            <Link className="sallesAfpaTitle navbar-brand d-none d-lg-inline-block" to="/admin/user">
                Gestion Utilisateurs
                </Link>
            <Link className="navbar-brand-two mx-auto d-lg-none d-inline-block" to="/admin/user">
                <i className="icImmeuble fas fa-users"></i>
            </Link>
            <div className="w-100 text-right">
                <button className="navbar-toggler custom-toggler" type="button" data-toggle="collapse" data-target="#myNavbar">
                    <span className="togglerIc fas fa-bars"></span>
                </button>
            </div>
        </div>
        <div id="listMob" className="collapse navbar-collapse flex-grow-1 text-right" id="myNavbar">
            <ul id="listMob" className="navbar-nav ml-auto flex-nowrap">
              
            <li className="listMob nav-item">
                    <Link to="/fonction" className="headerLink nav-link m-2 menu-item">Liste Fonctions</Link>
                </li>
                <li className="listMob nav-item">
                    <Link to="/admin/user/new" className="headerLink nav-link m-2 menu-item">Ajouter un Utilisateur</Link>
                </li>
                <li className="listMob nav-item">
                    <Link to="/admin/user" className=" headerLink nav-link m-2 menu-item">Liste des Utilisateurs</Link>
                </li>
               
                <li className="listMob nav-item">
                <Link to="/" onClick={deconnexionOnClick} className="deconnexionLink nav-link m-2 menu-item"><span style={{color:'red'}} class="fa fa-power-off"></span></Link> 
                </li>

            </ul>
        </div>
    </nav>
             
    );
}

