import React, { useState, useEffect,Fragment } from 'react';
import Header from '../Components/Header'
import TableauSalle from '../Components/TableauSalle'
import Filtre from '../Components/FiltreListeSalle'
import { MDBRow, MDBCol } from 'mdbreact';
import {  useToasts } from 'react-toast-notifications';
import {useParams,useHistory,Link} from 'react-router-dom';

export default function ListeSalle() {

    const [salles, setSalles] = useState([]);
    const { addToast } = useToasts();
    const [show, setShow] = useState(false)
    const [nbPage, setNbPage] = useState(0);
    const { page } = useParams();
    const[pageUrl, setPageUrl] = useState( page ? page : 1);
    const history = useHistory();

    const [filtres,setFiltres] = useState({
        "tabTypeSalle":[],
        "actif":"",
        "capacite":"",
        "batiment":[],
        "date":"",
        "dateDebut":new Date().toISOString().slice(0,10),
        "dateFin":new Date().toISOString().slice(0,10),

        

    })

    const getSalles =  async () => {
        let response = await fetch(`${process.env.REACT_APP_API_URL}/salle`, {
            method: "GET",
            headers: {
                'Accept': 'application/json',
                "Content-type": "application/json; charset=UTF-8",
                'Authorization': sessionStorage.token
            }
        })
            .catch(error => {console.log(error);}); 
            if (response && response.status===200) {
                let responseData = await response.json();
                setShow(true)
                setSalles(responseData);
            }else if(response && response.status===401){
                addToast("authentification requise", { appearance: 'error',autoDismiss: true,autoDismissTimeout : 1000  , onDismiss:(id)=>{history.push('/')}})
               
              } else {
                addToast("erreur serveur", { appearance: 'error',autoDismiss: true  })
            }
    }

   async function filtrer(){
    let pageUrl1 = page ? page : 1;
        let response = await fetch(`${process.env.REACT_APP_API_URL}/sallefiltre/${pageUrl1}`, {
            method: "POST",
            body: JSON.stringify(filtres),
            headers: {
                'Accept': 'application/json',
                "Content-type": "application/json; charset=UTF-8",
                'Authorization': sessionStorage.token
            }
        })
            .catch(error => {console.log(error);}); 
            if (response && response.status===200) {
                let responseData = await response.json();
                setShow(true)
                setSalles(responseData.liste);
                setNbPage(responseData.nbPage);
            }else if(response && response.status===401){
                addToast("authentification requise", { appearance: 'error',autoDismiss: true,autoDismissTimeout : 1000  , onDismiss:(id)=>{history.push('/')}})
               
              } else {
                addToast("erreur serveur", { appearance: 'error',autoDismiss: true  })
            }
    }

    
    useEffect(() => {
        setPageUrl( page ? page : 1)
        filtrer();
    }, [page]);


    function pagination (){
        let retour=[];
        for(let i=1;i<=nbPage;i++){
            retour.push(<li className={"page-item " + (i==pageUrl ? 'active' : '')} ><a className="page-link" onClick={ ()=>history.push("/listesalle/"+i)}>{i}</a></li>)
        }
        return retour;
    }

    return <Fragment>
        <Header />
    {!show ? (
      <div></div>
    ) : ( <div>
        <MDBRow>
            <MDBCol md="2" className="mb-6">
                <Filtre filtres={filtres} filtrer={filtrer}  />
            </MDBCol>
            <MDBCol md="10" className="mb-6">
                <TableauSalle salles={salles}  />
                <nav aria-label="...">
            <ul className="pagination justify-content-center">
                <li className={"page-item " + ((pageUrl-1)<1 ? 'disabled' : '')}>
                <Link className="page-link" to={"/listesalle/"+(pageUrl-1)} >&laquo;</Link>
                </li> 
                {pagination()}
                <li className={"page-item " + ((parseInt(pageUrl)+1)>nbPage ? 'disabled' : '')}>
                    <Link className="page-link" to={"/listesalle/"+(parseInt(pageUrl)+1)}>&raquo;</Link>
                </li>
            </ul>
        </nav>
            </MDBCol>
        </MDBRow> </div>)
    }</Fragment>
}