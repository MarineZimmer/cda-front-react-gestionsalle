import React, { useState, useEffect } from 'react';
import { MDBCol, MDBRow} from 'mdbreact';
import Header from '../Components/Header'
import FormNewReservation from '../Components/FormCreationReservation'
import ListeSalleDispo from '../Components/TableauSalleReservation'
import {  useToasts } from 'react-toast-notifications';
import {Link,useHistory} from 'react-router-dom';
export default function PageNewReservation(){

    const[messagesErreur,setMessagesErreur]=useState([]);
    const[salles,setSalles]=useState([]);
    const[reservation,setReservation] = useState({
      "id":"0",
      "dateDebut":"",
      "dateFin":"",
      "nomReservation":"",
      "salle":{}
    })

    const { addToast } = useToasts();

    const[page,setPage]=useState(1);
    const[nbPage,setNbPage]=useState(1);

    const[load,setLoad]=useState(false);
    const history = useHistory();
    async function rechercheSalle(reservation){
        
       let response = await  fetch(`${process.env.REACT_APP_API_URL}/salledispo/${page}`,{
        method: "POST",
        body: JSON.stringify(reservation),
        headers: {
          'Accept': 'application/json',
          "Content-type": "application/json; charset=UTF-8",
          'Authorization': sessionStorage.token
        }
      })
      .catch(error => console.log(error));
      if(response && response.status===200){
        let responseData = await response.json();
        setSalles(responseData.liste);
        setNbPage(responseData.nbPage);
        setMessagesErreur([]);
        
      }else if(response && response.status===400){
        let responseData = await response.json();
        setMessagesErreur(responseData);
        setSalles([]);
       } else if(response && response.status===401){
          addToast("authentification requise", { appearance: 'error',autoDismiss: true,autoDismissTimeout : 1000  , onDismiss:(id)=>{history.push('/')}})
         
        }else{
          addToast("erreur serveur", { appearance: 'error',autoDismiss: true  })
        }
      }

      useEffect(() => {
        if(load){
        rechercheSalle(reservation);
      }
      setLoad(true)
      },[page,reservation])
    
  
    return <div>
        <Header/>
        <MDBRow>
        <MDBCol md="2" className="mb-6">
        <FormNewReservation rechercheSalle={rechercheSalle} reservation={reservation} setReservation={setReservation} messagesErreur={messagesErreur}/>
      
        </MDBCol>
        <MDBCol md="10" className="mb-6">
        {messagesErreur.aucuneSalle}
        <ListeSalleDispo salles={salles} reservation={reservation}  setReservation={setReservation} page={page} setPage={setPage} nbPage={nbPage} setLoad={setLoad} />
        </MDBCol>
        </MDBRow>
    </div>
}